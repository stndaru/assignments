package assignments.assignment3;

class MataKuliah {
    
    private String nama;
    private int kapasitas;
    private int kapasitasSekarang;
    private Dosen dosen;
    private Mahasiswa[] daftarMahasiswa;

    public MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
        this.kapasitasSekarang = 0;
    }

    public String getName(){
        return this.nama;
    }

    public Dosen getDosen(){
        return this.dosen;
    }

    public int getKapasitasSekarang(){
        return this.kapasitasSekarang;
    }

    public int getKapasitas(){
        return this.kapasitas;
    }

    public Mahasiswa[] getDaftarMahasiswa(){
        return this.daftarMahasiswa;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // finds empty erray in daftarMahasiswa and adds the mahasiswa

        int emptyIndex = this.obtainEmptyArraySpace();
        this.daftarMahasiswa[emptyIndex] = mahasiswa;
        this.kapasitasSekarang++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        int studentIndex = this.findMahasiswa(mahasiswa);
        this.daftarMahasiswa[studentIndex] = null;
        for(int i = studentIndex; i < this.daftarMahasiswa.length; i++){
            if (i == this.daftarMahasiswa.length-1) {
                this.daftarMahasiswa[i] = null;
                break;
            }
            this.daftarMahasiswa[i] = this.daftarMahasiswa[i+1];
            if(this.daftarMahasiswa[i+1] == null) break;
        }
        this.kapasitasSekarang--;
    }

    public void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    public void dropDosen() {
        this.dosen = null;
    }

    private int obtainEmptyArraySpace(){
        for(int i = 0; i < this.daftarMahasiswa.length; i++){
            if (this.daftarMahasiswa[i] == null) {
                return i;
            }
        }
        return 999;
    }

    private int findMahasiswa(Mahasiswa mahasiswa) {
        for(int i = 0; i < this.daftarMahasiswa.length; i++){
            if (mahasiswa.getName().equals(this.daftarMahasiswa[i].getName())) return i;    
        }
        return 999;
    }

    public boolean isCapacityFull(){
        if(this.kapasitasSekarang == this.kapasitas) return true;
        return false;
    }

    public String toString() {
        return this.nama;
    }
}