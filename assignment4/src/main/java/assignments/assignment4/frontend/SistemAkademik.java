package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

// NOTES //
// All sorts are done
// using selection sort, reference obtained from
// https://www.geeksforgeeks.org/selection-sort/

// I gained insight from Yudha Haris Purnama and Muhammad Agil Ghifari
// for customizations

public class SistemAkademik {

    
    public static void main(String[] args) { 
        new SistemAkademikGUI();
    }
}

class SistemAkademikGUI extends JFrame{
    private static ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<Mahasiswa>();
    private static ArrayList<MataKuliah> daftarMataKuliah = new ArrayList<MataKuliah>();
    public static Font fontGeneral = new Font("Century Gothic", Font.PLAIN , 14);
    public static Font fontTitle = new Font("Century Gothic", Font.BOLD, 20);

    public static Color mainBG = new Color(3,9,44);
    public static Color secBG = new Color(12,18,51);
    public static Color btnColorRed = new Color(249,81,108);
    public static Color btnColorBlue = new Color(73,132,249);
    public static Color btnColorOrange = new Color(254,155,58);

    public SistemAkademikGUI(){

        // Membuat Frame
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 700);
        frame.setTitle("Administrator - Sistem Akademik");
        
        new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
        frame.setResizable(false);
        frame.setVisible(true);  

    }
}
