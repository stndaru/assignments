package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

// imported from http://javaswingcomponents.blogspot.com/2012/05/how-to-create-simple-hinttextfield-in.html
// set custom TextField which has hints inside of it

public class HintTextField extends JTextField {  

    public static Color mainBG = new Color(3,9,44);
    public static Color secBG = new Color(12,18,51);
    public static Color btnColorBlue = new Color(73,132,249);
    public static Color greyFade = new Color(113,115,122);
    public static Color greyFadeLighter = new Color(155,158,169);
    public static Color btnColorGreen = new Color(56,199,101);
  
    Font gainFont = new Font("Tahoma", Font.PLAIN, 12);  
    Font lostFont = new Font("Tahoma", Font.ITALIC, 12);
    
  
  public HintTextField(final String hint, JSeparator sep, JLabel label) {  

    // set text and color
    setText(hint);  
    setFont(lostFont);  
    setForeground(greyFade);
    
    // set size and alignment uniformity
    setAlignmentX(Component.CENTER_ALIGNMENT);
    setMaximumSize(new Dimension(320, 30));
    setMinimumSize(new Dimension(320, 30));
    setBackground(secBG);
    setBorder(null);
    setCaretColor(Color.WHITE);
  
    this.addFocusListener(new FocusAdapter() {  
  
      @Override  
      public void focusGained(FocusEvent e) {  
        // determines what happened when field is clicked
        sep.setBackground(btnColorBlue);
        label.setForeground(Color.WHITE);

        if (getText().equals(hint)) {  
          setText("");  
          setFont(gainFont);
          setForeground(Color.WHITE);
          
        } else {  
          setText(getText());  
          setFont(gainFont);  
          setForeground(Color.WHITE);
        }  
      }  
  
      @Override  
      public void focusLost(FocusEvent e) {
        // determines what happened when field is not clicked

        if (getText().equals(hint)|| getText().length()==0) { 
          sep.setBackground(Color.WHITE); 
          setText(hint);
          setFont(lostFont); 
          setForeground(greyFade);
          label.setForeground(greyFadeLighter);  
        
        } else {
          sep.setBackground(btnColorGreen);  
          setText(getText());  
          setFont(gainFont);  
          setForeground(Color.WHITE);
          label.setForeground(Color.WHITE);  
        }  
      }  
    });  
  
  }  
}  