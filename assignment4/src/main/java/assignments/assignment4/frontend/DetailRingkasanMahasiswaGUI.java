package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {

    private JPanel panelNorth;
    private JPanel panelCenter;
    private JPanel panelSouth;

    public static Color mainBG = new Color(3,9,44);
    public static Color secBG = new Color(12,18,51);
    public static Color btnColorBlue = new Color(73,132,249);
    public static Color greyFade = new Color(155,158,169);
    public static Color btnColorRed = new Color(230,53,101);
    public static Color btnColorGreen = new Color(56,199,101);

    Font gainFont = new Font("Tahoma", Font.PLAIN, 12);  
    Font lostFont = new Font("Tahoma", Font.ITALIC, 12);

    Font content = new Font("Arial", Font.PLAIN, 12);

    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // initiate labels
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.WHITE);

        JLabel footerLabel = new JLabel();
        footerLabel.setText("Created by: Stefanus Ndaru - 2006526812");
        footerLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        footerLabel.setHorizontalAlignment(JLabel.CENTER);
        footerLabel.setForeground(greyFade);

        JLabel nama = new JLabel();
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        nama.setForeground(Color.WHITE);
        nama.setText("Nama: " + mahasiswa.getNama());

        JLabel npm = new JLabel();
        npm.setAlignmentX(Component.CENTER_ALIGNMENT);
        npm.setForeground(Color.WHITE);
        npm.setText("NPM: " + mahasiswa.getNpm());

        JLabel jurusan = new JLabel();
        jurusan.setAlignmentX(Component.CENTER_ALIGNMENT);
        jurusan.setForeground(Color.WHITE);
        jurusan.setText("Jurusan: " + mahasiswa.getJurusan());

        JLabel daftarMK = new JLabel();
        daftarMK.setAlignmentX(Component.CENTER_ALIGNMENT);
        daftarMK.setForeground(btnColorBlue);
        daftarMK.setText("Daftar Mata Kuliah: ");

        JLabel sks = new JLabel();
        sks.setAlignmentX(Component.CENTER_ALIGNMENT);
        sks.setForeground(Color.WHITE);
        sks.setText("Total SKS: " + mahasiswa.getTotalSKS());

        JLabel irs = new JLabel();
        irs.setAlignmentX(Component.CENTER_ALIGNMENT);
        irs.setForeground(btnColorGreen);
        irs.setText("Hasil Pengecekan IRS: ");

        JLabel temp1 = new JLabel();
        temp1.setAlignmentX(Component.CENTER_ALIGNMENT);
        temp1.setFont(content);
        temp1.setForeground(greyFade);

        JLabel temp2 = new JLabel();
        temp2.setAlignmentX(Component.CENTER_ALIGNMENT);
        temp2.setFont(content);
        temp2.setForeground(greyFade);
        
        // initiate button
        JButton back = new CustomButton("Selesai", btnColorRed, btnColorRed, Color.WHITE, btnColorRed, true);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setSize(500, 700);
                frame.setVisible(true);
            }
        });

        // initiate panels
        panelNorth = new JPanel();
        panelCenter = new JPanel();
        panelSouth = new JPanel();

        frame.setLayout(new BorderLayout());
        
        // north panel layout
        panelNorth.setBorder(BorderFactory.createEmptyBorder(20,100,20,100));
        panelNorth.setBackground(mainBG);

        // center panel layout
        panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.PAGE_AXIS));
        panelCenter.setBorder(BorderFactory.createEmptyBorder(20,5,20,5));
        panelCenter.setBackground(secBG);

        // south panel layout
        panelSouth.setBackground(mainBG);
        panelSouth.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        // north panel content
        panelNorth.add(titleLabel);

        // center panel layout
        panelCenter.add(nama);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(npm);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(jurusan);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(daftarMK);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));

        // iterate daftarMatakuliah by creating JLabel array and iteration
        if(mahasiswa.getMataKuliah()[0] == null){
            temp1.setText("Belum ada mata kuliah yang diambil.");
            panelCenter.add(temp1);
            panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        }
        else{
            JLabel[] label = new JLabel[mahasiswa.getMataKuliah().length];

            for(int i = 0; i < mahasiswa.getBanyakMatkul(); i++){
                if(mahasiswa.getMataKuliah()[i] == null) break;

                label[i] = new JLabel(i+1 + ". " + mahasiswa.getMataKuliah()[i]);
                label[i].setFont(content);
                label[i].setForeground(Color.WHITE);
                label[i].setAlignmentX(Component.CENTER_ALIGNMENT);
                panelCenter.add(label[i]);
                panelCenter.add(Box.createRigidArea(new Dimension(5,5)));
            }

            panelCenter.add(Box.createRigidArea(new Dimension(5,10)));
        }

        panelCenter.add(sks);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(irs);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));

        // iterate daftarMasalahIRS by creating JLabel array and iteration
        if(mahasiswa.getMasalahIRS()[0] == null){
            temp2.setText("IRS tidak bermasalah.");
            panelCenter.add(temp2);
        }
        else{
            irs.setForeground(btnColorRed);
            JLabel[] labelIRS= new JLabel[mahasiswa.getMasalahIRS().length];
            
            for(int i = 0; i < mahasiswa.getBanyakMasalahIRS(); i++){
                if(mahasiswa.getMasalahIRS()[i] == null) break;

                labelIRS[i] = new JLabel(i+1 + ". " + mahasiswa.getMasalahIRS()[i]);
                labelIRS[i].setFont(content);
                labelIRS[i].setForeground(Color.WHITE);
                labelIRS[i].setAlignmentX(Component.CENTER_ALIGNMENT);
                panelCenter.add(labelIRS[i]);
                panelCenter.add(Box.createRigidArea(new Dimension(5,5)));
            }
        }

        panelCenter.add(Box.createRigidArea(new Dimension(5,45)));
        panelCenter.add(back);

        // south panel content
        panelSouth.add(footerLabel);

        // add panel to frame
        frame.add(panelNorth, BorderLayout.NORTH);
        frame.add(panelCenter, BorderLayout.CENTER);
        frame.add(panelSouth, BorderLayout.SOUTH);
    }
}
