package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {

    private JPanel panelNorth;
    private JPanel panelCenter;
    private JPanel panelSouth;

    public static Color mainBG = new Color(3,9,44);
    public static Color secBG = new Color(12,18,51);
    public static Color btnColorBlue = new Color(73,132,249);
    public static Color greyFade = new Color(155,158,169);
    public static Color btnColorRed = new Color(230,53,101);

    Font gainFont = new Font("Tahoma", Font.PLAIN, 12);  
    Font lostFont = new Font("Tahoma", Font.ITALIC, 12);

    Font content = new Font("Arial", Font.PLAIN, 12);

    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // initiate labels
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan MataKuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.WHITE);

        JLabel footerLabel = new JLabel();
        footerLabel.setText("Created by: Stefanus Ndaru - 2006526812");
        footerLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        footerLabel.setHorizontalAlignment(JLabel.CENTER);
        footerLabel.setForeground(greyFade);

        JLabel nama = new JLabel();
        nama.setAlignmentX(Component.CENTER_ALIGNMENT);
        nama.setForeground(Color.WHITE);
        nama.setText("Nama Mata Kuliah: " + mataKuliah.getNama());

        JLabel kode = new JLabel();
        kode.setAlignmentX(Component.CENTER_ALIGNMENT);
        kode.setForeground(Color.WHITE);
        kode.setText("Kode: " + mataKuliah.getKode());

        JLabel sks = new JLabel();
        sks.setAlignmentX(Component.CENTER_ALIGNMENT);
        sks.setForeground(Color.WHITE);
        sks.setText("SKS: " + mataKuliah.getSKS());

        JLabel jumlah = new JLabel();
        jumlah.setAlignmentX(Component.CENTER_ALIGNMENT);
        jumlah.setForeground(Color.WHITE);
        jumlah.setText("Jumlah Mahasiswa: " + mataKuliah.getJumlahMahasiswa());

        JLabel kapasitas = new JLabel();
        kapasitas.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitas.setForeground(Color.WHITE);
        kapasitas.setText("Kapasitas: " + mataKuliah.getKapasitas());

        JLabel daftarMhs = new JLabel();
        daftarMhs.setAlignmentX(Component.CENTER_ALIGNMENT);
        daftarMhs.setForeground(btnColorBlue);
        daftarMhs.setText("Daftar Mahasiswa: ");

        JLabel temp1 = new JLabel();
        temp1.setAlignmentX(Component.CENTER_ALIGNMENT);
        temp1.setFont(content);
        temp1.setForeground(greyFade);

        // initiate buttons
        JButton back = new CustomButton("Selesai", btnColorRed, btnColorRed, Color.WHITE, btnColorRed, true);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setSize(500, 700);
                frame.setVisible(true);
            }
        });

        // initiate panels
        panelNorth = new JPanel();
        panelCenter = new JPanel();
        panelSouth = new JPanel();

        frame.setLayout(new BorderLayout());
        
        // north panel layout
        panelNorth.setBorder(BorderFactory.createEmptyBorder(20,100,20,100));
        panelNorth.setBackground(mainBG);

        // center panel layout
        panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.PAGE_AXIS));
        panelCenter.setBorder(BorderFactory.createEmptyBorder(20,5,20,5));
        panelCenter.setBackground(secBG);

        // south panel layout
        panelSouth.setBackground(mainBG);
        panelSouth.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        // north panel content
        panelNorth.add(titleLabel);

        // center panel layout
        panelCenter.add(nama);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(kode);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(sks);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(jumlah);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(kapasitas);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(daftarMhs);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));

        // iterate daftarMahasiswa by creating JLabel array and iteration
        if(mataKuliah.getDaftarMahasiswa()[0] == null){
            temp1.setText("Belum ada mahasiswa yang mengambil mata kuliah ini.");
            panelCenter.add(temp1);
            panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        }
        else{
            JLabel[] label = new JLabel[mataKuliah.getDaftarMahasiswa().length];

            for(int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++){
                if(mataKuliah.getDaftarMahasiswa()[i] == null) break;

                label[i] = new JLabel(i+1 + ". " + mataKuliah.getDaftarMahasiswa()[i].getNama());
                label[i].setFont(content);
                label[i].setForeground(Color.WHITE);
                label[i].setAlignmentX(Component.CENTER_ALIGNMENT);
                panelCenter.add(label[i]);
                panelCenter.add(Box.createRigidArea(new Dimension(5,5)));
            }

            panelCenter.add(Box.createRigidArea(new Dimension(5,10)));
        }

        panelCenter.add(Box.createRigidArea(new Dimension(5,45)));
        panelCenter.add(back);

        // south panel content
        panelSouth.add(footerLabel);

        // add panel to frame
        frame.add(panelNorth, BorderLayout.NORTH);
        frame.add(panelCenter, BorderLayout.CENTER);
        frame.add(panelSouth, BorderLayout.SOUTH);
        
    }
}
