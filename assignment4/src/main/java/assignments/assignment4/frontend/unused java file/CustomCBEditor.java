package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import javax.swing.plaf.basic.BasicComboBoxEditor;

import assignments.assignment4.backend.*;

// modified from https://www.codejava.net/java-se/swing/create-custom-gui-for-jcombobox

public class CustomCBEditor extends BasicComboBoxEditor{

    private JLabel label = new JLabel();
    private JPanel panel = new JPanel();
    private Object selectedItem;

    public static Color mainBG = new Color(3,9,44);
    public static Color secBG = new Color(12,18,51);
    public static Color btnColorBlue = new Color(73,132,249);
    public static Color greyFade = new Color(113,115,122);
    public static Color greyFadeLighter = new Color(155,158,169);
    public static Color btnColorGreen = new Color(56,199,101); 

    public CustomCBEditor(){
        label.setOpaque(false);
        label.setForeground(Color.WHITE);

        panel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.insets = new Insets(2, 5, 2, 2);

        // panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 2));
        panel.add(label, constraints);
        panel.setBackground(mainBG);
    }

    public Component getEditorComponent(){
        return this.panel;
    }

    public Object getItem(){
        return this.selectedItem.toString();
    }

    public void setItem(Object item){
        this.selectedItem = item;
        label.setText(item.toString());
    }
}