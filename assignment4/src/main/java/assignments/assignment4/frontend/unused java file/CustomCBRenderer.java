package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import javax.swing.ListCellRenderer;

import assignments.assignment4.backend.*;

// modified from https://www.codejava.net/java-se/swing/create-custom-gui-for-jcombobox

public class CustomCBRenderer extends JLabel implements ListCellRenderer{
    
    public static Color mainBG = new Color(3,9,44);
    public static Color secBG = new Color(12,18,51);
    public static Color btnColorBlue = new Color(73,132,249);
    public static Color greyFade = new Color(113,115,122);
    public static Color greyFadeLighter = new Color(155,158,169);
    public static Color btnColorGreen = new Color(56,199,101); 
    
    private JLabel label = new JLabel();
    
    public CustomCBRenderer(){
        label.setOpaque(true);
        //label.setAlignmentX(Component.LEFT_ALIGNMENT);

        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.insets = new Insets(2, 2, 2, 2);

        add(label, constraints);
        setBackground(mainBG);
        setForeground(Color.WHITE);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus){
        
        label.setText(value.toString());

        if(isSelected){
            label.setBackground(btnColorBlue);
        }
        else{
            label.setBackground(mainBG);
        }

        return this;
    }
}