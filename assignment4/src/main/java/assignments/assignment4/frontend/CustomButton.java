package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

// insight given from Yudha Haris Purnama
// custom button used in nearly all of the buttons

public class CustomButton extends JButton {

    public CustomButton(final String text, final Color bgActive, final Color bgPassive, final Color fgActive, final Color fgPassive, boolean secondary){

        // sets button text
        setText(text);
        
        // sets foreground color and component alignments
        setForeground(fgPassive);
        setAlignmentX(Component.CENTER_ALIGNMENT);

        // checks for button type, secondary or not
        // secondary will have no background while primary have
        if(secondary){
            setBorder(BorderFactory.createLineBorder(bgPassive));
            setBackground(null);
        }
        else{
            setBorder(null);
            setBackground(bgPassive);
        }

        // forces size uniformity
        setMinimumSize(new Dimension(200, 30));
        setPreferredSize(new Dimension(200, 30));
        setMaximumSize(new Dimension(200, 30));

    // creates custom hover effect, changes color
    // when mouse is hovering
    this.addMouseListener(new java.awt.event.MouseAdapter() {

        @Override
        public void mouseEntered(java.awt.event.MouseEvent evt) {
            setBackground(bgActive);
            setForeground(fgActive);
            setBorder(BorderFactory.createRaisedBevelBorder());
            
        }

        @Override
        public void mouseExited(java.awt.event.MouseEvent evt) {
            setBackground(bgPassive);
            setForeground(fgPassive);

            if(secondary){
                setBorder(BorderFactory.createLineBorder(bgPassive));
                setBackground(null);
            }
            else{
                setBorder(null);
                setBackground(bgPassive);
            }
        }
       });

    }
}