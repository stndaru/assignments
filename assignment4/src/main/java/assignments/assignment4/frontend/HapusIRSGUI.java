package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HapusIRSGUI {

    private JPanel panelNorth;
    private JPanel panelCenter;
    private JPanel panelSouth;

    public static Color mainBG = new Color(3,9,44);
    public static Color secBG = new Color(12,18,51);
    public static Color btnColorBlue = new Color(73,132,249);
    public static Color greyFade = new Color(155,158,169);
    public static Color btnColorRed = new Color(230,53,101);

    Font gainFont = new Font("Tahoma", Font.PLAIN, 12);  
    Font lostFont = new Font("Tahoma", Font.ITALIC, 12);

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // initiate labels
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Hapus IRS");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.WHITE);

        JLabel footerLabel = new JLabel();
        footerLabel.setText("Created by: Stefanus Ndaru - 2006526812");
        footerLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        footerLabel.setHorizontalAlignment(JLabel.CENTER);
        footerLabel.setForeground(greyFade);
        
        JLabel npm = new JLabel("Pilih NPM: ");
        npm.setAlignmentX(Component.CENTER_ALIGNMENT);
        npm.setForeground(greyFade);

        JLabel namaMatkul = new JLabel("Pilih Nama Mata Kuliah: ");
        namaMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaMatkul.setForeground(greyFade);

        // obtain arrays of npm and matakuliah in String data type
        String[] npmListData = getMhsArray(daftarMahasiswa);
        String[] mkListData = getMKArray(daftarMataKuliah);

        // initiate combobox
        JComboBox npmList = new JComboBox(npmListData);
        npmList.setSelectedIndex(0);
        npmList.setMaximumSize(new Dimension(320, 30));
        
        JComboBox mkList = new JComboBox(mkListData);
        mkList.setSelectedIndex(0);
        mkList.setMaximumSize(new Dimension(320, 30));

        // initiate buttons
        JButton add = new CustomButton("Hapus", Color.WHITE, btnColorBlue, btnColorBlue, Color.WHITE, false);
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(npmList.getSelectedItem().equals("Pilih NPM") || 
                    mkList.getSelectedItem().equals("Pilih mata kuliah")){
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh Field");
                }
                else{
                    try{
                        // obtain data from combobox, downcast from Object to String or Long first
                        Mahasiswa mhs = getMahasiswa(Long.parseLong(String.valueOf(npmList.getSelectedItem())), daftarMahasiswa);
                        MataKuliah mk = getMataKuliah(String.valueOf(mkList.getSelectedItem()), daftarMataKuliah);
                        hapusIRS(mhs, mk, frame, daftarMahasiswa, daftarMataKuliah);
                    }
                    catch (NumberFormatException er){
                        JOptionPane.showMessageDialog(null, "Terdapat kesalahan input");
                    }
                }

                // resets the field
                npmList.setSelectedIndex(0);
                mkList.setSelectedIndex(0);
            }
        });
        
        JButton back = new CustomButton("Kembali", btnColorRed, btnColorRed, Color.WHITE, btnColorRed, true);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });

        // initiate panels
        panelNorth = new JPanel();
        panelCenter = new JPanel();
        panelSouth = new JPanel();

        frame.setLayout(new BorderLayout());
        
        // north panel layout
        panelNorth.setBorder(BorderFactory.createEmptyBorder(20,5,20,5));
        panelNorth.setBackground(mainBG);

        // center panel layout
        panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.PAGE_AXIS));
        panelCenter.setBorder(BorderFactory.createEmptyBorder(20,5,5,5));
        panelCenter.setBackground(secBG);

        // south panel layout
        panelSouth.setBackground(mainBG);
        panelSouth.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        // north panel content
        panelNorth.add(titleLabel);

        // center panel content
        panelCenter.add(npm);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(npmList);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(namaMatkul);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(mkList);
        panelCenter.add(Box.createRigidArea(new Dimension(5,45)));

        panelCenter.add(add);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(back);

        // south panel content
        panelSouth.add(footerLabel);

        // add panel to frame
        frame.add(panelNorth, BorderLayout.NORTH);
        frame.add(panelCenter, BorderLayout.CENTER);
        frame.add(panelSouth, BorderLayout.SOUTH);
    }

    private String[] getMKArray(ArrayList<MataKuliah> daftarMataKuliah){
        // selection sort
        
        String[] mkSorted = new String[daftarMataKuliah.size() + 1];

        mkSorted[0] = "Pilih mata kuliah";
        
        // copypaste data from Arraylist
        for(int i = 1; i < daftarMataKuliah.size() + 1; i++){
            mkSorted[i] = daftarMataKuliah.get(i-1).getNama();
        }

        // does the selection sort
        for(int i = 1; i < mkSorted.length-1; i++){
            if(mkSorted[i] == null) break;

            int min = i;
            for(int j = i+1; j < mkSorted.length; j++){
                if(mkSorted[j] == null) break;
                if(mkSorted[j].toLowerCase().compareTo(mkSorted[i].toLowerCase()) < 0 &&
                    mkSorted[j].toLowerCase().compareTo(mkSorted[min].toLowerCase()) < 0)
                        min = j;
            }
            if(i == min) continue;

            String temp = mkSorted[min];
            mkSorted[min] = mkSorted[i];
            mkSorted[i] = temp;

        }

        return mkSorted;
    }

    private String[] getMhsArray(ArrayList<Mahasiswa> daftarMahasiswa){
        // selection sort

        long[] mhsSorted = new long[daftarMahasiswa.size()];
        String[] mhsSortedString = new String[mhsSorted.length + 1];

        mhsSortedString[0] = "Pilih NPM";

        // copypaste data from Arraylist
        for(int i = 0; i < daftarMahasiswa.size(); i++){
            mhsSorted[i] = daftarMahasiswa.get(i).getNpm();
        }

        // does the selection sort
        for(int i = 0; i < mhsSorted.length-1; i++){
            if(mhsSorted[i] == 0) break;

            int min = i;
            for(int j = i+1; j < mhsSorted.length; j++){
                if(mhsSorted[j] == 0) break;
                if(mhsSorted[j] < mhsSorted[i] &&
                    mhsSorted[j] < mhsSorted[min])
                        min = j;
            }
            if(i == min) continue;

            long temp = mhsSorted[min];
            mhsSorted[min] = mhsSorted[i];
            mhsSorted[i] = temp;
        }

        // turns the long data back into Strings
        for(int i = 0; i < mhsSorted.length; i++){
            mhsSortedString[i+1] = String.valueOf(mhsSorted[i]);
        }

        return mhsSortedString;
    }

    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }

    private void hapusIRS(Mahasiswa mhs, MataKuliah mk, JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // deletes the matkul the student currently having
        
        String status = mhs.dropMatkul(mk);
        JOptionPane.showMessageDialog(null, status);

        if(status.substring(0, 10).equals("[BERHASIL]")){
            frame.getContentPane().removeAll();
            frame.repaint();

            new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            frame.setVisible(true);
        }
    }
}
