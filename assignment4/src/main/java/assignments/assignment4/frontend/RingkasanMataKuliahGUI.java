package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {
    
    private JPanel panelNorth;
    private JPanel panelCenter;
    private JPanel panelSouth;

    public static Color mainBG = new Color(3,9,44);
    public static Color secBG = new Color(12,18,51);
    public static Color btnColorBlue = new Color(73,132,249);
    public static Color greyFade = new Color(155,158,169);
    public static Color btnColorRed = new Color(230,53,101);

    Font gainFont = new Font("Tahoma", Font.PLAIN, 12);  
    Font lostFont = new Font("Tahoma", Font.ITALIC, 12);

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // initiate labels
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setForeground(Color.WHITE);

        JLabel footerLabel = new JLabel();
        footerLabel.setText("Created by: Stefanus Ndaru - 2006526812");
        footerLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        footerLabel.setHorizontalAlignment(JLabel.CENTER);
        footerLabel.setForeground(greyFade);
        
        JLabel matkul = new JLabel("Pilih Nama Mata Kuliah: ");
        matkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        matkul.setForeground(greyFade);

        // initiate arrays for matakuliah list
        String[] mkListData = getMKArray(daftarMataKuliah);

        // initiate combobox
        JComboBox mkList = new JComboBox(mkListData);
        mkList.setSelectedIndex(0);
        mkList.setMaximumSize(new Dimension(320, 30));

        // initiate buttons
        JButton add = new CustomButton("Lihat", Color.WHITE, btnColorBlue, btnColorBlue, Color.WHITE, false);
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(mkList.getSelectedItem().equals("Pilih mata kuliah")){
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh Field");
                }
                else{
                    try{
                        MataKuliah mk = getMataKuliah(String.valueOf(mkList.getSelectedItem()), daftarMataKuliah);
                        
                        frame.getContentPane().removeAll();
                        frame.repaint();

                        new DetailRingkasanMataKuliahGUI(frame, mk, daftarMahasiswa, daftarMataKuliah);
                        frame.setVisible(true);
                        frame.pack();
                    }
                    catch (NumberFormatException er){
                        JOptionPane.showMessageDialog(null, "Terdapat kesalahan input");
                    }
                }

                // resets the field
                mkList.setSelectedIndex(0);
            }
        });

        JButton back = new CustomButton("Selesai", btnColorRed, btnColorRed, Color.WHITE, btnColorRed, true);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                frame.repaint();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.setVisible(true);
            }
        });

        // initiate panels
        panelNorth = new JPanel();
        panelCenter = new JPanel();
        panelSouth = new JPanel();

        frame.setLayout(new BorderLayout());

        // north panel layout
        panelNorth.setBorder(BorderFactory.createEmptyBorder(20,5,20,5));
        panelNorth.setBackground(mainBG);

        // center panel layout
        panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.PAGE_AXIS));
        panelCenter.setBorder(BorderFactory.createEmptyBorder(20,5,5,5));
        panelCenter.setBackground(secBG);

        // south panel layout
        panelSouth.setBackground(mainBG);
        panelSouth.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        // north panel content
        panelNorth.add(titleLabel);

        // center panel
        panelCenter.add(matkul);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(mkList);
        panelCenter.add(Box.createRigidArea(new Dimension(5,45)));

        panelCenter.add(add);
        panelCenter.add(Box.createRigidArea(new Dimension(5,15)));
        panelCenter.add(back);

        // south panel content
        panelSouth.add(footerLabel);

        // add panel to frame
        frame.add(panelNorth, BorderLayout.NORTH);
        frame.add(panelCenter, BorderLayout.CENTER);
        frame.add(panelSouth, BorderLayout.SOUTH);
        
    }

    private String[] getMKArray(ArrayList<MataKuliah> daftarMataKuliah){
        // selection sort
        
        String[] mkSorted = new String[daftarMataKuliah.size() + 1];

        mkSorted[0] = "Pilih mata kuliah";
        
        // copypaste data from Arraylist
        for(int i = 1; i < daftarMataKuliah.size() + 1; i++){
            mkSorted[i] = daftarMataKuliah.get(i-1).getNama();
        }

        // does the selection sort
        for(int i = 1; i < mkSorted.length-1; i++){
            if(mkSorted[i] == null) break;

            int min = i;
            for(int j = i+1; j < mkSorted.length; j++){
                if(mkSorted[j] == null) break;
                if(mkSorted[j].toLowerCase().compareTo(mkSorted[i].toLowerCase()) < 0 &&
                    mkSorted[j].toLowerCase().compareTo(mkSorted[min].toLowerCase()) < 0)
                        min = j;
            }
            if(i == min) continue;

            String temp = mkSorted[min];
            mkSorted[min] = mkSorted[i];
            mkSorted[i] = temp;

        }

        return mkSorted;
    }

    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
}
