package assignments.assignment2;

import java.util.Scanner;
import java.util.Arrays;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm){
        // obtain object mahasiswa with the same npm, null otherwise
        for(int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i].getNPM() == npm) return daftarMahasiswa[i];
        }
        return null;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah){
        // obtain object matakuliah with the sanme name, null otherwise
        for(int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i].getNama().equals(namaMataKuliah)) return daftarMataKuliah[i];
        }
        return null;
    }

    private String getMasalahIRS(Mahasiswa mahasiswa){
        // outputs the string containing IRS problems
        String output = "";

        // if there's no problem
        if(mahasiswa.getArrMasalahIRS()[0] == null && mahasiswa.getArrMasalahIRS()[1] == null)
            output += "IRS tidak bermasalah.";

        // if there's no SKS amount problem
        else if(mahasiswa.getArrMasalahIRS()[0] == null){
            output += ("1. " + mahasiswa.getArrMasalahIRS()[1]);
            for(int i = 2; i < mahasiswa.getArrMasalahIRS().length; i++){
                if(mahasiswa.getArrMasalahIRS()[i] != null){
                    output += ("\n" + i + ". " + mahasiswa.getArrMasalahIRS()[i]);
                }
            }
        }
        // if there is SKS amount problem
        else{
            output += ("1. " + mahasiswa.getArrMasalahIRS()[0]);
            for(int i = 1; i < mahasiswa.getArrMasalahIRS().length; i++){
                if(mahasiswa.getArrMasalahIRS()[i] != null){
                    int n = i + 1;
                    output += ("\n" + n + ". " + mahasiswa.getArrMasalahIRS()[i]);
                }
            }
        }
        return output;
    }

    private void addNewMahasiswa(String nama, long npm){
        // to add new mahasiswa to put into array, finds empty slot first
        for(int i = 0; i < daftarMahasiswa.length; i++){
            if(daftarMahasiswa[i] == null){
                daftarMahasiswa[i] = new Mahasiswa(nama, npm);
                break;
            }
        }
    }

    private void addNewMatkul(String kode, String nama, int sks, int kapasitas){
        // to add new matakuliah to put into array, finds empty slot first
        for(int i = 0; i < daftarMataKuliah.length; i++){
            if(daftarMataKuliah[i] == null){
                daftarMataKuliah[i] = new MataKuliah(kode, nama, sks, kapasitas);
                break;
            }
        }
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        // get mahasiswa based on npm
        Mahasiswa mahasiswa = this.getMahasiswa(npm);

        // a little bit of note, if matkul already max, adding more just prints error messages
        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();

            // get matakuliah and then adds them
            MataKuliah matakuliah = this.getMataKuliah(namaMataKuliah);
            mahasiswa.addMatkul(matakuliah);

        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

       // get mahasiswa based on npm and checks if it has matkul added or not
        Mahasiswa mahasiswa = this.getMahasiswa(npm);
        if(mahasiswa.getJumlahMatkul() == 0) System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");

        else{
            System.out.print("Banyaknya Matkul yang Di-drop: ");
            int banyakMatkul = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan nama matkul yang di-drop:");
            for(int i=0; i<banyakMatkul; i++){
                System.out.print("Nama matakuliah " + i+1 + " : ");
                String namaMataKuliah = input.nextLine();
                
                // get matakuliah and then drops them
                MataKuliah matakuliah = this.getMataKuliah(namaMataKuliah);
                mahasiswa.dropMatkul(matakuliah);
            }
            System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
        }
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());

        Mahasiswa mahasiswa = this.getMahasiswa(npm);

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + mahasiswa.getNama());
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + mahasiswa.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        // prints the matkuls
        System.out.println(mahasiswa.printMatkul());

        System.out.println("Total SKS: " + mahasiswa.getTotalSKS());
        
        System.out.println("Hasil Pengecekan IRS:");
        
        // prints masalah IRS
        System.out.println(this.getMasalahIRS(mahasiswa));
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();

        MataKuliah matakuliah = this.getMataKuliah(namaMataKuliah);
        
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + matakuliah.getNama());
        System.out.println("Kode: " + matakuliah.getKode());
        System.out.println("SKS: " + matakuliah.getSKS());
        System.out.println("Jumlah mahasiswa: " + matakuliah.getKapasitasSekarang());
        System.out.println("Kapasitas: " + matakuliah.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
        
        // prints matkuls participants
        System.out.println(matakuliah.printMahasiswaInClass());
        
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }


    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            String kode = dataMatkul[0];
            String nama = dataMatkul[1];
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            // creates new matkul and adds them to the array
            this.addNewMatkul(kode, nama, sks, kapasitas);
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            String nama = dataMahasiswa[0];
            long npm = Long.parseLong(dataMahasiswa[1]);
            // creates new mahasiswa and adds them to the array
            this.addNewMahasiswa(nama, npm);
        }
        
        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }


    
}
