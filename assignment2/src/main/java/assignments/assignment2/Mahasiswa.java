package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS = new String[20];
    private int totalSKS;
    private String nama;
    private String jurusan;
    private String kodeJurusan;
    private long npm;
    private int jumlahMatkul;

    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.jumlahMatkul = 0;
        this.totalSKS = 0;
        this.npm = npm;
        this.obtainJurusan(npm);
    }

    // getters
    public String getJurusan(){
        return this.jurusan;
    }
    
    public String getNama(){
        return this.nama;
    }

    public long getNPM(){
        return this.npm;
    }

    public int getTotalSKS(){
        return this.totalSKS;
    }

    public int getJumlahMatkul(){
        return this.jumlahMatkul;
    }

    public String[] getArrMasalahIRS(){
        return this.masalahIRS;
    }

    public boolean checkNoClassAdded(){
        // checks if there's no class added or not
        if(this.mataKuliah[0] == null) return true;
        return false;
    }

    public void addMatkul(MataKuliah mataKul){
        if (!this.checkClassAdded(mataKul)){
            if (!this.checkClassCapacity(mataKul)){
                if (!this.checkClassAmount()){
                    this.jumlahMatkul++;

                    // finds empty slot in the array
                    int indexMatkul = this.findEmptyArrayMatkul();
                    this.mataKuliah[indexMatkul] = mataKul;

                    mataKul.addMahasiswa(this);

                    this.totalSKS += mataKul.getSKS();

                    // checks the IRS
                    this.cekIRS(mataKul);
                }
            }
        }
    }

    public void dropMatkul(MataKuliah matkul){
        int index = this.findMatkul(matkul);

        // index 999 is when the matkul is not found in the array
        if (index == 999) System.out.println("[DITOLAK] " + matkul.getNama() + " belum pernah diambil.");
        else {
            // replace the dropped matkul with null, and then shifts
            // the entire array after the dropped one to the left
            // this way it will stay organized

            this.mataKuliah[index] = null;

            // shifter
            for(int i = index; i < this.mataKuliah.length; i++){
                if(i == this.mataKuliah.length-1) {
                    this.mataKuliah[i] = null;
                    break;
                }
                this.mataKuliah[i] = this.mataKuliah[i+1];
                if(this.mataKuliah[i+1] == null) break;
            }

            this.totalSKS -= matkul.getSKS();
            this.jumlahMatkul--;
            this.validateSKS();
            
            // checks if the matkul was having problem with jurusan incompability
            matkul.dropMahasiswa(this);
            if(!this.kodeJurusan.equals("CS"))
                if(!this.kodeJurusan.equals(matkul.getKode())) removeMasalahMatkul(matkul);
        }
    }


    public void cekIRS(MataKuliah mataKul){
        this.validateJurusanAdd(mataKul);
        this.validateSKS();
    }

    public String toString(){
        return this.nama;
    }

    public String printMatkul(){
        // outputs a string contains a list of taken matkul
        
        String output = "";
        if(this.checkNoClassAdded()) output += "Belum ada mata kuliah yang diambil";
        else{
            output += ("1. " + this.mataKuliah[0].getNama());
            for(int i = 1; i < this.mataKuliah.length; i++){
                if(this.mataKuliah[i] != null){
                    int n = i+1;
                    output += ("\n" + n + ". " + mataKuliah[i].getNama());
                }
            }
        }
        return output;
    }

    public void obtainJurusan(long npm){
        String kode = Long.toString(npm);
        switch (Integer.parseInt(kode.substring(2,4))) {
            case 01:
                this.jurusan = "Ilmu Komputer";
                this.kodeJurusan = "IK";
                break;
            case 02:
                this.jurusan = "Sistem Informasi";
                this.kodeJurusan = "SI";
                break;
            default:
                break;
        }
    }

    public boolean checkClassAdded(MataKuliah matkul){
        // check if class has been added to the array, true if added

        for(int i = 0; i < this.mataKuliah.length; i++){
            if(this.mataKuliah[i] == null) break;
            else if(this.mataKuliah[i].getNama().equals(matkul.getNama())){
                System.out.println("[DITOLAK] " + matkul.getNama() + " telah diambil sebelumnya.");
                return true;
            }
        }
        return false;
    }

    public boolean checkClassCapacity(MataKuliah matkul){
        if (matkul.isCapacityFull()){
            System.out.println("[DITOLAK] " + matkul.getNama() + " telah penuh kapasitasnya.");
            return true;
        }
        return false;
    }

    public boolean checkClassAmount(){
        // check amount of class that has been added

        if (this.jumlahMatkul == 10){
            System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
            return true;
        }
        return false;
    }

    public int findEmptyArrayMatkul(){
        for(int i = 0; i < this.mataKuliah.length; i++){
            if (this.mataKuliah[i] == null) return i;
        }
        return 999;
    }

    public int findEmptyArrayMasalah(){
        for(int i = 1; i < this.masalahIRS.length; i++){
            if (this.masalahIRS[i] == null) return i;
        }
        return 999;
    }

    public void removeMasalahMatkul(MataKuliah matkul){
        // removes problems with matkul incompatibility with jurusan
        // iterate over array, find the matkul that had problems
        // after that replace it with null, and shift problems after to the left
        
        for (int i = 1; i < this.masalahIRS.length; i++){
            if(this.masalahIRS[i] == null) break;
            if(this.masalahIRS[i].substring(12, (12 + matkul.getNama().length())).equals(matkul.getNama())) {
                this.masalahIRS[i] = null;
                for (int j = i; j < this.masalahIRS.length; j++){
                    if (i == this.masalahIRS.length-1){
                        this.masalahIRS[i] = null;
                        break; 
                    }
                    this.masalahIRS[j] = this.masalahIRS[j+1];
                    if(this.masalahIRS[j+1] == null) break;
                }
                break;
            }  
        }
    }

    public int findMatkul(MataKuliah matkul){
        // find matkul based on the matkul name
        
        for (int i = 0; i < this.mataKuliah.length; i++){
            if (this.mataKuliah[i] == null) break;
            if (this.mataKuliah[i].getNama().equals(matkul.getNama())) return i;
        }
        return 999;
    }

    private void validateJurusanAdd(MataKuliah mataKul){
        // adds the incompatible matkul with jurusan to masalahIRS, if any

        if (!mataKul.getKode().equals(this.kodeJurusan) && !mataKul.getKode().equals("CS")) {
            int indexMasalah = this.findEmptyArrayMasalah();
            this.masalahIRS[indexMasalah] = "Mata Kuliah " + mataKul.getNama() + " tidak dapat diambil jurusan " + this.kodeJurusan;
        }
    }

    private void validateSKS(){
        // adds the SKS problem to masalahIRS, if any

        if (this.totalSKS > 24) {
            if (this.masalahIRS[0] == null) this.masalahIRS[0] = "SKS yang Anda ambil lebih dari 24";
        }
        else {
            this.masalahIRS[0] = null;
        }
    }
}
